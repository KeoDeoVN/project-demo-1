package manaki.web.shop.model.entity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Cart {
	
	private List<ProductCart> products;
	
	public Cart() {
		this.products = new ArrayList<ProductCart> ();
	}
	
	public List<ProductCart> getProducts() {
		return this.products;
	}
	
	public int getAmount(String productId) {
		for (ProductCart pc : products) {
			if (pc.getProductId().equals(productId)) return pc.getAmount();
		}
		return 0;
	}
	
	public void addProduct(String productId, int amount) {
		for (ProductCart pc : products) {
			if (pc.getProductId().equals(productId)) {
				pc.setAmount(pc.getAmount() + amount);
				return;
			}
		}
		products.add(new ProductCart(productId));
	}
	
	public void removeProduct(String productId) {
		this.products.removeIf(pc -> pc.getProductId().equals(productId));
	}
	
	public void removeProduct(String productId, int amount) {
		Iterator<ProductCart> i = products.iterator();
		while (i.hasNext()) {
			ProductCart pc = i.next();
			if (pc.getProductId().equals(productId)) {
				int current = pc.getAmount();
				if (current <= amount) {
					i.remove();
					return;
				}
				pc.setAmount(pc.getAmount() - amount);
				return;
			}
		}
	}
	
	public float getSumPrice() {
		float sum = 0;
		for (ProductCart pc : products) {
			sum += pc.getProductSumPrice();
		}
		return sum;
	}
	
}
