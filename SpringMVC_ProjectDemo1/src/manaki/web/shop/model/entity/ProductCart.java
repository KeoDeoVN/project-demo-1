package manaki.web.shop.model.entity;

import manaki.web.shop.controller.ProductController;

public class ProductCart {
	
	private String productId;
	private int amount; 
	
	public ProductCart(String productId) {
		this.productId = productId;
		this.amount = 1;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	public String getProductName() {
		Product product = ProductController.getInst().getService().getProductById(this.productId);
		return product.getProductName();
	}
	
	public float getProductSumPrice() {
		Product product = ProductController.getInst().getService().getProductById(this.productId);
		return product.getPrice() * this.amount;
	}
	
}
