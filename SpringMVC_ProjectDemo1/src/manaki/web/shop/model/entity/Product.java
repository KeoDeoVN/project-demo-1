package manaki.web.shop.model.entity;

public class Product {
	
	private String productId;
	private String productName;
	private String title;
	private String description;
	//O day de image la String
	private String image;
	private float price;
	
	public Product() {}
	
	public Product(String productId, String productName, String title, String description, String image, float price) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.title = title;
		this.description = description;
		this.image = image;
		this.price = price;
	}

	public String getProductId() {
		return productId;
	}
	
	public void setProductId(String productId) {
		this.productId = productId;
	}
	
	public String getProductName() {
		return productName;
	}
	
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getImage() {
		return image;
	}
	
	public void setImage(String image) {
		this.image = image;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
	
	
}
