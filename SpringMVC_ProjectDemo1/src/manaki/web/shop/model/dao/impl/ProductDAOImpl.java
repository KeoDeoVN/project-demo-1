package manaki.web.shop.model.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import manaki.web.shop.model.dao.IProductDAO;
import manaki.web.shop.model.entity.Product;
import manaki.web.shop.utils.Connections;

@Repository
public class ProductDAOImpl implements IProductDAO {
	
	private static final String QUERY_FROM_ID = "SELECT ProductId, ProductName, ProductTitle, ProductDescription, ProductImage, ProductPrice FROM Product WHERE ProductId=?";
	private static final String QUERY_ALL = "SELECT ProductId, ProductName, ProductTitle, ProductDescription, ProductImage, ProductPrice FROM Product";
	
	private static final String CREATE = "INSERT INTO Product VALUES (?, ?, ?, ?, ?, ?)";
	
	@Override
	public Product getProductById(String id) {
		Connection conn = Connections.openConnection();
		
		try {
			PreparedStatement ps = conn.prepareStatement(QUERY_FROM_ID);
			ps.setString(1, id);
			
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				String productId = rs.getString("ProductId");
				String productName = rs.getString("ProductName");
				String title = rs.getString("ProductTitle");
				String description = rs.getString("ProductDescription");
				String image = rs.getString("ProductImage");
				float price = rs.getFloat("ProductPrice");
				
				return new Product(productId, productName, title, description, image, price);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			Connections.closeConnection();
		}
		
		return null;
	}

	@Override
	public List<Product> getAllProducts() {
		Connection conn = Connections.openConnection();
		List<Product> list = new ArrayList<Product> ();
		
		try {
			PreparedStatement ps = conn.prepareStatement(QUERY_ALL);
			
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				String productId = rs.getString("ProductId");
				String productName = rs.getString("ProductName");
				String title = rs.getString("ProductTitle");
				String description = rs.getString("ProductDescription");
				String image = rs.getString("ProductImage");
				float price = rs.getFloat("ProductPrice");
				
				list.add(new Product(productId, productName, title, description, image, price));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			Connections.closeConnection();
		}
		
		return list;
	}

	@Override
	public void createProduct(Product product) {
		Connection conn = Connections.openConnection();
		
		try {
			PreparedStatement ps = conn.prepareStatement(CREATE);
			ps.setString(1, product.getProductId());
			ps.setString(2,  product.getProductName());
			ps.setString(3, product.getTitle());
			ps.setString(4, product.getDescription());
			ps.setString(5, product.getImage());
			ps.setFloat(6, product.getPrice());
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			Connections.closeConnection();
		}

	}

}
