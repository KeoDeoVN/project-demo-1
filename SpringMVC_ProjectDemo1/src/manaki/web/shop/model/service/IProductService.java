package manaki.web.shop.model.service;

import java.util.List;

import manaki.web.shop.model.entity.Product;

public interface IProductService {
	
	public Product getProductById(String id);
	public List<Product> getAllProducts();
	public void createProduct(Product product);
	
}
