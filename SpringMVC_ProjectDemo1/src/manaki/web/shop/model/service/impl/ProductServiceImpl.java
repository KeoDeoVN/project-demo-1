package manaki.web.shop.model.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import manaki.web.shop.model.dao.IProductDAO;
import manaki.web.shop.model.entity.Product;
import manaki.web.shop.model.service.IProductService;

@Service
public class ProductServiceImpl implements IProductService {
	
	@Autowired
	private IProductDAO productDAO;

	@Override
	public Product getProductById(String id) {
		return productDAO.getProductById(id);
	}

	@Override
	public List<Product> getAllProducts() {
		return productDAO.getAllProducts();
	}

	@Override
	public void createProduct(Product product) {
		productDAO.createProduct(product);
	}
	
	
	
}
