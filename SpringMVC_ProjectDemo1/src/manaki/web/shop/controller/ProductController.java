package manaki.web.shop.controller;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import manaki.web.shop.model.entity.Cart;
import manaki.web.shop.model.entity.Product;
import manaki.web.shop.model.service.IProductService;

@Controller
@RequestMapping(value = "/product-controller")
public class ProductController {
	
	// Static
	private static ProductController inst;
	public static ProductController getInst() {
		return inst;
	}
	
	// 
	
	@Autowired
	private IProductService productService;
	
	private Cart cart;
	
	public ProductController() {
		this.cart = new Cart();
		
		inst = this;
	}
	
	public IProductService getService() {
		return this.productService;
	}
	
	@RequestMapping(value = "/cart.htm")
	public ModelAndView showCart() {
		ModelAndView mav = new ModelAndView("cart");
		mav.addObject("cart", cart);
		mav.addObject("service", productService);
		
		return mav;
	}
	
	@RequestMapping(value = "/cart-add.htm")
	public String addToCart(HttpServletRequest request) {
		String productId = request.getParameter("id");
		
		this.cart.addProduct(productId, 1);
		return "redirect:cart.htm";
	}
	
	@RequestMapping(value = "/cart-subtract.htm")
	public String subtractFormCart(HttpServletRequest request) {
		String productId = request.getParameter("id");
		this.cart.removeProduct(productId, 1);
		return "redirect:cart.htm";
	}
	
	@RequestMapping(value = "/cart-remove.htm")
	public String removeFormCart(HttpServletRequest request) {
		String productId = request.getParameter("id");
		this.cart.removeProduct(productId);
		return "redirect:cart.htm";
	}
	
	@RequestMapping(value = "/products.htm")
	public ModelAndView readAll(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("products");
		List<Product> products = productService.getAllProducts();
		mav.addObject("products", products);
		
		return mav;
	}
	
	@RequestMapping(value = "/create-product.htm")
	public ModelAndView redirectCreate() {
		ModelAndView mav = new ModelAndView("create-product");
		
		mav.addObject("product", new Product());
		
		return mav;
	}
	
	@RequestMapping(value = "/show-product.htm")
	public ModelAndView showProductInfo(HttpServletRequest request) {
		String id = request.getParameter("productId");
		Product product = productService.getProductById(id);
		
		ModelAndView mav = new ModelAndView("product-info");
		
		mav.addObject("product", product);
		
		return mav;
	}
	
	@RequestMapping(value = "/do-create.htm", method = RequestMethod.POST)
	public String doCreate(@ModelAttribute(value = "product") Product product, 
			@RequestParam(name = "productImage") MultipartFile productImage,
			HttpServletRequest request) {
		
		product.setImage(productImage.getOriginalFilename());
		productService.createProduct(product);
		saveImage(request, productImage);
		
		return "redirect:products.htm";
	}
	
	private String saveImage(HttpServletRequest request, MultipartFile multi) {
		try {
			byte[] bytes = multi.getBytes();
			String path = request.getServletContext().getRealPath("/images");
			File file = new File(path);
			File dest = new File(file.getAbsoluteFile() + "/" + multi.getOriginalFilename());
			if (!dest.exists()) {
				Files.write(dest.toPath(), bytes, StandardOpenOption.CREATE);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return multi.getOriginalFilename();
	}
	
}
