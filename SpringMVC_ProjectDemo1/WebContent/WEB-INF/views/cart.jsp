<%@page import="manaki.web.shop.model.entity.Product"%>
<%@page import="manaki.web.shop.controller.ProductController"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath }/styles/style.css">
    <title>Giỏ đồ</title>
</head>
<body>
    <!-- Menu -->
    <div class="menu">
        <ul class="main-menu">
			<li><a href="products.htm">Mua hàng</a></li>
			<li><a href="cart.htm">Giỏ hàng</a></li>
			<li><a href="create-product.htm">Tạo mới</a></li>
        </ul>
    </div>

    <table class="cart-table">
        <tr style="font-weight: bold;">
            <td>Mã hàng</td>
            <td>Tên hàng</td>
            <td>Số lượng</td>
            <td>Thành tiền</td>
            <td>Hành động</td>
        </tr>
        <% int sum = 0; %>
        
        <!-- Products -->
        <c:forEach items="${cart.products }" var="pc">
	        <tr>
	            <td>${pc.productId }</td>
	            <td>${pc.productName }</td>
	            <td>
	            	<a href="cart-subtract.htm?id=${pc.productId }"><input type="button" value="-"></a>
	                <input type="text" style="width: 25px;" value="${pc.amount}" readonly>
	                <a href="cart-add.htm?id=${pc.productId }"><input type="button" value="+"></a>
	            </td>
	            <td>${pc.productSumPrice }$</td>
	            <td>
	                <a href="cart-remove.htm?id=${pc.productId }">Loại bỏ</a>
	            </td>
	        </tr>
        </c:forEach>
        
        <!-- Sum -->
        <tr>
            <td colspan="5">
                <p>Tổng tiền: ${cart.sumPrice }$</p>
            </td>
        </tr>
    </table>
    
    <a href="products.htm" style="margin-left: 345px; font-size: 20px;">Tiếp tục mua hàng...</a>

</body>
</html>