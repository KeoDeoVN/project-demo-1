<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet"
	href="${pageContext.servletContext.contextPath }/styles/style.css">
<script type="text/javascript"
	src="${pageContext.servletContext.contextPath }/libraries/ckeditor/ckeditor.js"></script>
<title>Thêm sản phẩm mới</title>
</head>
<body>
	<!-- Menu -->
	<div class="menu">
		<ul class="main-menu">
			<li><a href="products.htm">Mua hàng</a></li>
			<li><a href="cart.htm">Giỏ hàng</a></li>
			<li><a href="">Tạo mới</a></li>
		</ul>
	</div>

	<f:form action="do-create.htm" modelAttribute="product" method="post" enctype="multipart/form-data">
		<div class="create-container">
			<table id="create">
				<tr>
					<td style="width: 150px;">Nhập ID</td>
					<td style="width: 500px;"><f:input path="productId" /></td>
				</tr>
				<tr>
					<td>Nhập Tên</td>
					<td><f:input path="productName" /></td>
				</tr>
				<tr>
					<td>Nhập Giá</td>
					<td><f:input path="price" /></td>
				</tr>
				<tr>
					<td>Tải ảnh lên</td>
					<td><input type="file" name="productImage"></td>
				</tr>
				<tr>
					<td>Nhập tiêu đề</td>
					<td><f:textarea path="title" style="width: 400px;"/></td>
				</tr>
				<tr>
					<td>Thiết kế Mô tả</td>
					<td><f:textarea path="description" id="description" /></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" value="Thêm sản phẩm"
						style="width: 120px; height: 30px"></td>
				</tr>
			</table>
		</div>
	</f:form>

	<script type="text/javascript">
		var desc = CKEDITOR.replace("description");
	</script>

</body>
</html>