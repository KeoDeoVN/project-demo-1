<%@page import="manaki.web.shop.utils.Utils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="${pageContext.servletContext.contextPath }/styles/style.css">
<title>Mua hàng</title>
</head>
<body>
	<!-- Menu -->
	<div class="menu">
		<ul class="main-menu">
			<li><a href="">Mua hàng</a></li>
			<li><a href="cart.htm">Giỏ hàng</a></li>
			<li><a href="create-product.htm">Tạo mới</a></li>
		</ul>
	</div>

	<c:forEach items="${products }" var="product">
		<!-- Product  -->
		<div class="product" onclick="location.href='show-product.htm?productId=${product.productId}';">
			<div class="image">
				<img src="${pageContext.request.contextPath }/images/${product.image }" alt="">
			</div>
			<div class="desc">
				<div class="title">${product.title }</div>
				<div class="name" style="display: flex; height: 20px;">
					<p style="margin-right: 5px;">Mặt hàng:</p>
					<p>${product.productName }</p>
				</div>
				<div class="price">
					<p style="font-weight: bold;">${product.price }$</p>
				</div>
				<div class="short-desc" style="margin-bottom: 10px;">
					${product.description }</div>
				<div class="buy-now">
					<a href="cart-add.htm?id=${product.productId }">Đừng mua, vẫn mua thì bấm vào</a>
				</div>
			</div>
		</div>
	</c:forEach>


</body>
</html>