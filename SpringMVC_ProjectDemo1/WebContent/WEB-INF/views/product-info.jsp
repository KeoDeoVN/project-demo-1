<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="${pageContext.servletContext.contextPath }/styles/style.css">
    <title>Thông tin sản phẩm</title>
</head>
<body>
    <!-- Menu -->
    <div class="menu">
        <ul class="main-menu">
			<li><a href="products.htm">Mua hàng</a></li>
			<li><a href="">Giỏ hàng</a></li>
			<li><a href="create-product.htm">Tạo mới</a></li>
        </ul>
    </div>
    
    <!-- Product  -->
    <div class="product-info">
        <div class="title">${product.title }</div>
        <div class="name">${product.productName }</div>
        <div class="image" style="margin-top: 20px; margin-bottom: 20px;">
            <img src="${pageContext.request.contextPath }/images/${product.image }" alt="">
        </div>
        <div class="desc">
            ${product.description } 
        </div>
    </div>
</body>
</html>